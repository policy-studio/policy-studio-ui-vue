# UI library of Vue components with StoryBook

### Instalation
```sh
npm i -S @policystudio/policy-studio-ui-vue
```

### Import the library
```javascript
// src/initComponents.js

import PolicyStudioUiVue from '@policystudio/policy-studio-ui-vue'
// ...
Vue.use(PolicyStudioUiVue) 
```
### Import CSS
Add in your css file

```css
/* src/assets/css/index.css */

@import '~@policystudio/policy-studio-ui-vue/src/assets/scss/tailwind.css'
```

### Run development mode (watch changes)
```npm run serve```

### Integrated tailwind only
```npm run watch-prod-tailwind```

### Website
[ui.policystudio.co](https://ui.policystudio.co/)


### Integrate with local application
Inside the policy-studio-ui-vue project folder
``` npm link ```

Navigate to the application folder that you wants to link with the library
```` npm link @policystudio/policy-studio-ui-vue ````

