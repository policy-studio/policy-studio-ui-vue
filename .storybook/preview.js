
/**
 * Import tailwind css file (bundle) based on environment
 */
const cssEnvFolder = (process.env.STORYBOOK_ENV === 'production') ? 'dist' : 'temp'
require( `../${cssEnvFolder}/css/psui_styles.css`)
console.warn(`Using tailwind css file(bundle) from: ../${cssEnvFolder}/css/psui_styles.css`)

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
}