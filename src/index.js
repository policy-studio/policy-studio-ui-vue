import PsButton from './components/buttons/PsButton.vue'
import PsCheckbox from './components/controls/PsCheckbox.vue'
import PsRadioButton from './components/controls/PsRadioButton.vue'
import PsSlider from './components/controls/PsSlider.vue'
import PsSwitch from './components/controls/PsSwitch.vue'
import PsToggle from './components/controls/PsToggle.vue'
import PsInput from './components/forms/PsInput.vue'
import PsDropdown from './components/forms/PsDropdown.vue'
import PsDropdownList from './components/forms/PsDropdownList.vue'
import PsInputTextArea from './components/forms/PsInputTextArea.vue'
import PsInputSelect from './components/forms/PsInputSelect.vue'
import PsDialog from './components/notifications/PsDialog.vue'
import PsToast from './components/notifications/PsToast.vue'
import PsTabHeader from './components/tabs/PsTabHeader.vue'
import PsAccordion from './components/accordion/PsAccordion.vue'
import PsAccordionItem from './components/accordion/PsAccordionItem.vue'
import PsChips from './components/chips/PsChips.vue'
import PsDataTable from './components/datatable/PsDataTable.vue'
import PsDataTableItem from './components/datatable/PsDataTableItem.vue'
import PsIcon from './components/ui/PsIcon.vue'
import PsDotLoader from './components/ui/PsDotLoader.vue'
import PsTooltip from './components/tooltip/PsTooltip.vue'
import PsRichTooltip from './components/tooltip/PsRichTooltip.vue'
import PsDialogTooltip from './components/tooltip/PsDialogTooltip.vue'
import PsDraggable from './components/controls/PsDraggable.vue'
import PsCardInfos from './components/badges-and-tags/PsCardInfos.vue'
import PsChartLegend from './components/badges-and-tags/PsChartLegend.vue'
import PsInlineSelector from './components/controls/PsInlineSelector.vue'
import PsScrollBar from './components/playground/PsScrollBar.vue'



export default {
  install(Vue) {
    Vue.component('PsButton', PsButton)
    Vue.component('PsCheckbox', PsCheckbox)
    Vue.component('PsDialog', PsDialog)
    Vue.component('PsToast', PsToast)
    Vue.component('PsTabHeader', PsTabHeader)
    Vue.component('PsRadioButton', PsRadioButton)
    Vue.component('PsSlider', PsSlider)
    Vue.component('PsSwitch', PsSwitch)
    Vue.component('PsInput', PsInput)
    Vue.component('PsToggle', PsToggle)
    Vue.component('PsAccordion', PsAccordion)
    Vue.component('PsAccordionItem', PsAccordionItem)
    Vue.component('PsChips', PsChips)
    Vue.component('PsDataTable', PsDataTable)
    Vue.component('PsDataTableItem', PsDataTableItem)
    Vue.component('PsIcon', PsIcon)
    Vue.component('PsDotLoader', PsDotLoader)
    Vue.component('PsTooltip', PsTooltip)
    Vue.component('PsRichTooltip', PsRichTooltip)
    Vue.component('PsDialogTooltip', PsDialogTooltip)
    Vue.component('PsDraggable', PsDraggable)
    Vue.component('PsCardInfos', PsCardInfos)
    Vue.component('PsChartLegend', PsChartLegend)
    Vue.component('PsInlineSelector', PsInlineSelector)
    Vue.component('PsInputTextArea', PsInputTextArea)
    Vue.component('PsInputSelect', PsInputSelect)
    Vue.component('PsDropdown',PsDropdown)
    Vue.component('PsDropdownList', PsDropdownList)
    Vue.component('PsScrollBar', PsScrollBar)
    
    Vue.directive('click-outside', {
      bind: function (el, binding, vnode) {
        el.clickOutsideEvent = function (event) {
          // here I check that click was outside the el and his children
          if (!(el == event.target || el.contains(event.target))) {
            // and if it did, call method provided in attribute value
            vnode.context[binding.expression](event)
          }
        }
        document.body.addEventListener('click', el.clickOutsideEvent)
      },
      unbind: function (el) {
        document.body.removeEventListener('click', el.clickOutsideEvent)
      },
    })
  },
}

export {
  PsButton,
  PsCheckbox,
  PsRadioButton,
  PsSlider,
  PsSwitch,
  PsToggle,
  PsInput,
  PsDialog,
  PsToast,
  PsTabHeader,
  PsAccordion,
  PsAccordionItem,
  PsChips,
  PsDataTable,
  PsDataTableItem,
  PsIcon,
  PsDotLoader,
  PsTooltip,
  PsRichTooltip,
  PsDialogTooltip,
  PsDraggable,
  PsCardInfos,
  PsChartLegend,
  PsInlineSelector,
  PsInputTextArea,
  PsInputSelect,
  PsDropdown,
  PsDropdownList,
  PsScrollBar
}

