import PsDataTable, { alignment } from '../components/datatable/PsDataTable.vue'
import PsDataTableItem from '../components/datatable/PsDataTableItem.vue'

export default {
    title: 'Components/DataTable',
    component: PsDataTable,
    subcomponents: { PsDataTableItem },
    argTypes: {
        align: { control: { type: 'select', options: alignment } },
    }
}

const Template = (args, { argTypes }) => ({
    props: Object.keys(argTypes),
    components:  {PsDataTable},
    template: `
        <div style="width:400px;">
            <PsDataTable v-bind="$props" />
        </div>
        `
})

const RichTemplate = (args, { argTypes } ) => ({
    props: Object.keys(argTypes),
    components: {PsDataTable, PsDataTableItem},
    template: `
            <div style="width:400px">    
                <PsDataTable v-bind="$props">
                    <PsDataTableItem v-bind="$props"/>
               </PsDataTable>
            </div>   
    `
})


export const Simple = Template.bind({})
Simple.args = {
    header: ['year', 'month', 'sale'],
    data: [{ year: '1992', month: '12', sale: '1000.00' }, { year: '1989', month: '02', sale: '1200.00' }],
    footer: ['Footer 1', 'Footer 2', 'Footer 3'],
    type: 'simple',
}

export const Rich = RichTemplate.bind({})
Rich.args = {
    header: ['header 1', 'header 2', 'header 3'],
    data:[[ [ 20, -3],  [  20,  2], [  20,  2] ], { header4: { value: 20, delta: 2}, header5: { value: 20, delta: 2}, header6: { value: 20, delta: 2} }],
    footer: ['Footer 1', 'Footer 2', 'Footer 3'],
    type: 'rich'
}