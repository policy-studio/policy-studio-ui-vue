import PsSlider from '../components/controls/PsSlider.vue'
export default {
  title: 'Components/Slider',
  component: PsSlider,
  argTypes: {
  },
}

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { PsSlider },
  template: `
  <div style="width: 500px;">
    <PsSlider v-bind="$props" :value.sync="value" />
  </div>
  `
})

export const Label = Template.bind({})
Label.args = {
  min: 1,
  max: 100,
  value: 30,
  label: 'Score Label'
}

export const NoLabel = Template.bind({})
NoLabel.args = {
  min: 1,
  max: 100,
  value: 15
}

export const Bubble = Template.bind({})
Bubble.args = {
  min: 1,
  max: 100,
  value: 23,
  bubble: true
}
