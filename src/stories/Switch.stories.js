import PsSwitch from '../components/controls/PsSwitch.vue'
export default {
  title: 'Components/Switch',
  component: PsSwitch,
}

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { PsSwitch },
  data(){
    return {
      dt: false

    }
  },
  template: `
    <div style='display: flex; gap:10px;'> 
      <div style='display: flex; gap: 10px; flex-direction: column; margin-right: 10px;'>
        <span>Size</span>
        <span>BIG</span>
        <span>SMALL</span>
      </div>
      <div style='display: flex; gap: 10px; flex-direction: column;'>
        <span>Disable</span>
        <PsSwitch v-bind="$props" v-model="dt" label='Switch1' disabled size='big'/>
        <PsSwitch v-bind="$props" v-model="dt" label='Switch2' disabled size='small'/>
      </div>
      <div style='display: flex; gap: 10px; flex-direction: column;'>
      <span>Enable</span>
        <PsSwitch v-bind="$props" label='Switch3' value size='big'/>
        <PsSwitch v-bind="$props" label='Switch4' value size='small'/>
      </div>
    </div>
  `
})

export const Default = Template.bind({})


