import PsCardInfos from '../components/badges-and-tags/PsCardInfos.vue'

export default {
  title: 'Components/CardInfos',
  component: PsCardInfos,
  argTypes: {},
}

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { PsCardInfos },
  template: '<PsCardInfos v-bind="$props" />',
})

export const CardInfos = Template.bind({})
CardInfos.args = {}
