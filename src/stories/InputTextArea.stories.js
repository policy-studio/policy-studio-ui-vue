import PsInputTextArea from '../components/forms/PsInputTextArea.vue'

export default {
    title: 'Components/Input Textarea',
    component: PsInputTextArea,
}

const Template = (args, {argTypes}) => ({
    props: Object.keys(argTypes),
    components: {PsInputTextArea},
    template: `
    <div style='background-color:#F3F6F9; width:400px; height: 100%; padding: 20px 20px; display: flex: display; flex-direction: column;'>
        <div style='display: flex; align-items:center; gap:30px;'>
            <h1>Default</h1>
            <PsInputTextArea v-bind='$props' placeholder='Optional placeholder' label='Drop us a line' rows="${10}" optionalLabel="Optional helper/feedback text"></PsInputTextArea>    
        </div>
        <div style='display: flex; align-items:center; gap:30px; margin-top:40px;'>
            <h1>Disable</h1>
            <PsInputTextArea v-bind='$props' placeholder='Optional placeholder' label='Drop us a line' rows="${10}" disabled ></PsInputTextArea>
        </div>
    </div>
    `
}) 

export const Default = Template.bind({})