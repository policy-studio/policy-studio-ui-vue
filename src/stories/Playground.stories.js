import PsScrollBar from '../components/playground/PsScrollBar.vue'

export default {
    title: 'Components/Playground',
    component: PsScrollBar
}

const Template =  (args, {argTypes}) => ({
    props: Object.keys(argTypes),
    components: { PsScrollBar},
    template: `
            <PsScrollBar v-bind='$props' scrollWidth='30'></PsScrollbar>
    `
})

export const ScrollBar = Template.bind({})