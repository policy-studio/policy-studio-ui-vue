import PsInput from '../components/forms/PsInput.vue'
export default {
  title: 'Components/Input',
  component: PsInput,
  argTypes: {
    disabled: { control: 'boolean' },
    required: { control: 'boolean' },
  },
}
 
const TemplateInputText = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { PsInput },
  data: () => {
    return {
      validator: {
        hasError: true,
        label: 'Error message!'
      }
    }
  },
  template: `    
    <div class="psui-p-8 psui-bg-gray-10 psui-grid psui-grid-cols-3 psui-gap-6">          
 
      <div class="psui-col-span-3 psui-mt-8">
        <h1 class="psui-font-bold psui-border-b psui-border-gray-30">PSInput Layout Default</h1>
      </div>
     
      <PsInput v-bind="{...$props, label: 'Resting with hint', placeholder: '', value: '' }" />
      <PsInput label="Resting without hint" />      
      <PsInput label="Resting with Placeholder" placeholder="This is a placeholder" />      
 
      <PsInput v-bind="{...$props, label: 'Focus'}" />
      <PsInput label="Typing" placeholder="Type to see the effect..." />
      <PsInput v-bind="{...$props, label: 'Active', active: true }" />
     
      <PsInput v-bind="{...$props, label: 'Error', hasError: true }" />
      <PsInput v-bind="{...$props, label: 'Error with custom message', hasError: 'Format invalid' }" />    
      <PsInput v-bind="{...$props, label: 'Prepend / Append ' }" >
        <template v-slot:append>
          Append
        </template>
        <template v-slot:prepend>
          Prepend
        </template>
      </PsInput>
 
      <PsInput label="Disabled" :disabled="true" value="100,000" />      
      <PsInput v-bind="{...$props, label: 'Disabled without value', disabled: true, value: '', hint: false }" />
      <PsInput v-bind="{...$props, label: 'Active/Disabled', disabled: true, active: true }" />      
 
     
 
      <div class="psui-col-span-3 psui-mt-8">
        <h1 class="psui-font-bold psui-border-b psui-border-gray-30">PSInput Layout Mini</h1>
      </div>
 
      <PsInput layout="mini" label="Resting with hint" hint="This is a hint" value="1,653" />      
      <PsInput layout="mini" label="Resting without hint" />      
      <PsInput layout="mini" label="Resting with Placeholder" placeholder="This is a placeholder" />      
 
      <PsInput layout="mini" v-bind="{...$props, label: 'Focus'}" />
      <PsInput layout="mini" label="Typing" placeholder="Type to see the effect..." />
      <PsInput layout="mini" v-bind="{...$props, label: 'Active', active: true }" />
     
      <PsInput layout="mini" v-bind="{...$props, label: 'Error', hasError: true }" />
      <PsInput layout="mini" v-bind="{...$props, label: 'Error with custom message', hasError: 'Format invalid' }" />
      <PsInput layout="mini" v-bind="{...$props, label: 'Prepend / Append ' }" >
        <template v-slot:append>
          Append
        </template>
        <template v-slot:prepend>
          Prepend
        </template>
      </PsInput>
 
      <PsInput layout="mini" label="Disabled" :disabled="true" value="100,000" />      
      <PsInput layout="mini" v-bind="{...$props, label: 'Disabled without value', disabled: true, value: '', hint: false }" />
      <PsInput layout="mini" v-bind="{...$props, label: 'Active/Disabled', disabled: true, active: true }" />      
 
    </div>
  `
})
 
const TemplateInputPassword = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { PsInput },
  data: () => {
    return {
      validator: {
        hasError: true,
        label: 'Error message!'
      }
    }
  },
  template: `    
    <div class="psui-p-8 psui-bg-gray-10 psui-grid psui-grid-cols-3 psui-gap-6">          
 
      <div class="psui-col-span-3 psui-mt-8">
        <h1 class="psui-font-bold psui-border-b psui-border-gray-30">PSInput Password Layout Default</h1>
      </div>
     
      <PsInput v-bind="{...$props, label: 'Resting with hint', placeholder: '', value: '', hint: 'Between 8 and 16 characters' }" >
        <template v-slot:append>
          <span class="material-icons-round psui-text-gray-50 psui-mr-2">
            visibility_off
          </span>
        </template>
      </PsInput>
 
      <PsInput label="Resting without hint" hint="Between 8 and 16 characters">
        <template v-slot:append>
          <span class="material-icons-round psui-text-gray-50 psui-mr-2">
            visibility_off
          </span>
        </template>
        </PsInput>
       
        <PsInput label="Resting with Placeholder" placeholder="This is a placeholder" hint="Between 8 and 16 characters">        
          <template v-slot:append>
            <span class="material-icons-round psui-text-gray-50 psui-mr-2">
              visibility_off
            </span>
          </template>
      </PsInput>
 
 
      <PsInput v-bind="{...$props, label: 'Focus'}" hint="Between 8 and 16 characters">
        <template v-slot:append>
          <span class="material-icons-round psui-text-gray-50 psui-mr-2">
            visibility_off
          </span>
        </template>
      </PsInput>
 
      <PsInput v-bind="{...$props, label: 'Typing', placeholder: 'Type to see the effect...', value: '' }" hint="Between 8 and 16 characters" >
        <template v-slot:append>
          <span class="material-icons-round psui-text-gray-50 psui-mr-2">
            visibility_off
          </span>
        </template>
      </PsInput>
 
      <PsInput v-bind="{label: 'Active', active: true }" value="Password123" hint="Between 8 and 16 characters">
        <template v-slot:append>
          <span class="material-icons-round psui-text-gray-50 psui-mr-2">
            visibility
          </span>
        </template>
      </PsInput>
     
      <PsInput v-bind="{...$props, label: 'Error', hasError: true }" hint="Between 8 and 16 characters" />
      <PsInput v-bind="{...$props, label: 'Error with custom message', hasError: 'Format invalid' }" />    
      <PsInput v-bind="{...$props, label: 'Prepend / Append ' }" hint="Between 8 and 16 characters">
        <template v-slot:append>
          Append
        </template>
        <template v-slot:prepend>
          Prepend
        </template>
      </PsInput>
 
      <PsInput label="Disabled" :disabled="true" value="100,000" hint="Between 8 and 16 characters">
        <template v-slot:append>
          <span class="material-icons-round psui-text-gray-50 psui-mr-2">
            visibility
          </span>
        </template>
      </PsInput>
 
      <PsInput v-bind="{...$props, label: 'Disabled without value', disabled: true, value: '', hint: false }">
        <template v-slot:append>
          <span class="material-icons-round psui-text-gray-50 psui-mr-2">
            visibility_off
          </span>
        </template>
      </PsInput>
 
      <PsInput v-bind="{...$props, label: 'Active/Disabled', disabled: true, active: true }" hint="Between 8 and 16 characters">
        <template v-slot:append>
          <span class="material-icons-round psui-text-gray-50 psui-mr-2">
            visibility_off
          </span>
        </template>
      </PsInput>      
    </div>
  `
})
 
const TemplateSlots = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { PsInput },
  template: `
    <div class="psui-p-8 psui-bg-gray-10">
      <PsInput v-bind="$props" class="psui-mb-4">
        <template v-slot:append>
          ft²
        </template>
      </PsInput>  
      <PsInput v-bind="$props" class="psui-mb-4">
        <template v-slot:prepend>
          ft²
        </template>
      </PsInput>  
      <PsInput v-bind="$props" class="psui-mb-4">
        <template v-slot:prepend>
          ft²
        </template>
        <template v-slot:append>
          ft²
        </template>
      </PsInput>
    </div>
  `
})
 
export const InputText = TemplateInputText.bind({})
InputText.args = {
  label: 'Input Text',
  placeholder: 'Placeholder',
  hint: 'Optional Assistive text',
  disabled: false,
  required: false,
  value: 10,
}
 
export const InputPassword = TemplateInputPassword.bind({})
InputPassword.args = {
  type: 'password',
  label: 'Password',
  hint: 'Optional Assistive text',
  disabled: false,
  required: false,
}
 
export const InputSlots = TemplateSlots.bind({})
InputSlots.args = {
  label: 'Label',
  placeholder: 'Placeholder',
  hint: 'Optional Assistive text',
  disabled: false,
  required: false,
}
