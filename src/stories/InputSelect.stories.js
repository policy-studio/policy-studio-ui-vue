import PsInputSelect from '../components/forms/PsInputSelect.vue'

export default {
    title: 'Components/InputSelect',
    component: PsInputSelect
}

const TemplateDefault = (args, {argTypes})=> ({
    props: Object.keys(argTypes),
    components: {PsInputSelect},
    template: `
  <div style='display:flex; gap: 30px;'>  
    <div style='width:300px; display:flex; flex-direction: column; gap:20px;'>
        <span>Default Select</span>
        <PsInputSelect v-bind="$props" label="Gender" keyLabel="title" keyValue="value" optionalLabel="Optional helper/feedback text"/>
        <PsInputSelect v-bind="$props" disabled label="Gender" keyLabel="title" keyValue="value" optionalLabel="Optional helper/feedback text"/>
    </div>
    <div style='width:90px; display:flex; flex-direction: column; gap:20px;'>
        <span>Mini Select</span>
        <PsInputSelect v-bind="$props" layout="mini" keyLabel="title" keyValue="value" optionalLabel="Optional helper/feedback text"/>
        <PsInputSelect v-bind="$props" layout="mini" disabled keyLabel="title" keyValue="value" optionalLabel="Optional helper/feedback text"/>
    </div>
  </div>
    `
})

export const Default = TemplateDefault.bind({})
Default.args = {
    items: [{title:'Text 1', value:'Value 1'}, {title:'Text 2', value:'Value 2'}]
}