import PsTooltip from '../components/tooltip/PsTooltip.vue'
import PsRichTooltip from '../components/tooltip/PsRichTooltip.vue'
import PsDialogTooltip from '../components/tooltip/PsDialogTooltip.vue'

export default {
    title: 'Components/Tooltip',
    component: PsTooltip,
    subcomponents: {PsRichTooltip, PsDialogTooltip}
}

const TemplateDialog = (args, {argTypes}) => ({
    props: Object.keys(argTypes),
    components: {PsDialogTooltip},
    template: `
    <div style='display: flex; justify-content: space-between;'>
        <PsDialogTooltip v-bind='$props' layout='white' title='Duct Sealing' buttonText='More Info' cssClass='psui-w-64'>
            <template v-slot:trigger>                      
                  <p>Trigger</p>                  
            </template>
            <template v-slot:content>
                   <p>Air seal all ductwork to a final duct leakage value of 10% of nominal airflow.</p> 
            </template>
        </PsDialogTooltip>
        <PsDialogTooltip v-bind='$props' layout='dark' title='Duct Sealing' buttonText='More Info' cssClass='psui-w-64'>
            <template v-slot:trigger>                      
                  <p>Trigger</p>                  
            </template>
            <template v-slot:content>
                   <p>Air seal all ductwork to a final duct leakage value of 10% of nominal airflow.</p> 
            </template>
        </PsDialogTooltip>
        <PsDialogTooltip v-bind='$props' layout='color' title='Duct Sealing' buttonText='More Info' cssClass='psui-w-64'>
            <template v-slot:trigger>                      
                  <p>Trigger</p>                  
            </template>
            <template v-slot:content>
                   <p>Air seal all ductwork to a final duct leakage value of 10% of nominal airflow.</p> 
            </template>
        </PsDialogTooltip>
    </div>
    `,
})

const TemplateRich = (args,{argTypes})=>({
    props: Object.keys(argTypes),
    components: {PsRichTooltip},
    template: `
    <div style='display: flex; justify-content: space-between;'>
        <PsRichTooltip v-bind='$props' layout='red' title='Measures that are not cost-effective cannot be added to a combination' cssClass='psui-w-64'>
            <template v-slot:trigger>
                <p>Trigger</p>                            
            </template>
            <template v-slot:content>
                <p>Here you can add an optional supporting text</p> 
            </template>
        </PsRichTooltip>
        <PsRichTooltip v-bind='$props' layout='blue' title='Select to create a combination of measures for your Policy' cssClass='psui-w-64'>
            <template v-slot:trigger>
                <p>Trigger</p>                            
            </template>
            <template v-slot:content>
                <p>Here you can add an optional supporting text</p> 
            </template>
        </PsRichTooltip>
        <PsRichTooltip v-bind='$props' layout='gray' title='Did not account for the cost of combustion safety testing' cssClass='psui-w-64'>
            <template v-slot:trigger>
                <p>Trigger</p>                            
            </template>
            <template v-slot:content>
                <p>Here you can add an optional supporting text</p> 
            </template>
        </PsRichTooltip>
    </div>
`
})

const TemplateDefault = (args, {argTypes}) => ({
props: Object.keys(argTypes),
components: {PsTooltip},
template: `
    <div style='display: flex; justify-content: space-between;'>
        <PsTooltip v-bind='$props'>
            <template v-slot:trigger>
                Trigger
            </template>
            <template v-slot:content>
                Basic Tooltip
            </template>
        </PsTooltip>
        <PsTooltip v-bind='$props'>
            <template v-slot:trigger>
                Trigger
            </template>
            <template v-slot:content>
                Basic Tooltip
            </template>
        </PsTooltip>
        <PsTooltip v-bind='$props'>
            <template v-slot:trigger>
                Trigger
            </template>
            <template v-slot:content>
                Basic Tooltip
            </template>
        </PsTooltip>
    </div>    
`
})

export const Dialog = TemplateDialog.bind({})

export const Rich = TemplateRich.bind({})

export const Default = TemplateDefault.bind({})
