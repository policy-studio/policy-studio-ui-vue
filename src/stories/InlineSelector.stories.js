import PsInlineSelector from '../components/controls/PsInlineSelector.vue'

export default {
    title: 'Components/InlineSelector',
    component: PsInlineSelector,
}

const TemplateDefault = (args, {argTypes}) => ({
    props: Object.keys(argTypes),
    components: {PsInlineSelector},
    template: `
    <div style="width: auto; margin-right: auto; display: flex; align-items: center;">  
        <PsInlineSelector v-bind="$props"></PsInlineSelector>
    </div>
        `
})

export const Default = TemplateDefault.bind({})