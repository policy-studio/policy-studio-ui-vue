import PsToast from '../components/notifications/PsToast.vue'
export default {
  title: 'Components/Toast',
  component: PsToast,
}

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { PsToast },
  template: `
  <div style="display: flex; gap: 30px;">
    <div style="display: flex; flex-direction: column; gap: 5px; width:500px;">
      <PsToast v-bind="$props" message="This is a info alert" layout="info" icon="info" fill="intense" >
        <p class="hover:psui-underline">Action 1</p>
        <p class="hover:psui-underline">Action 2</p>
      </PsToast>
      <PsToast v-bind="$props" message="This is a success alert" layout="success" icon="check_circle" >
        <p class="hover:psui-underline">Action 1</p>
        <p class="hover:psui-underline">Action 2</p>
      </PsToast>
      <PsToast v-bind="$props" message="This is a warning alert" layout="warning" icon="warning">
        <p class="hover:psui-underline">Action 1</p>
        <p class="hover:psui-underline">Action 2</p>
      </PsToast>
      <PsToast v-bind="$props" message="This is a an error alert" layout="error" icon="warning" >
        <p class="hover:psui-underline">Action 1</p>
        <p class="hover:psui-underline">Action 2</p>
      </PsToast>
    </div>
    <div style="display: flex; flex-direction: column; gap: 5px; width:500px;">
      <PsToast v-bind="$props" message="This is a info alert" layout="info" icon="info" fill="soft" >
        <p class="hover:psui-underline">Action 1</p>
        <p class="hover:psui-underline">Action 2</p>
      </PsToast>
      <PsToast v-bind="$props" message="This is a success alert" layout="success" icon="check_circle" fill="soft">
        <p class="hover:psui-underline">Action 1</p>
        <p class="hover:psui-underline">Action 2</p>
      </PsToast>
      <PsToast v-bind="$props" message="This is a warning alert" layout="warning" icon="warning" fill="soft">
        <p class="hover:psui-underline">Action 1</p>
        <p class="hover:psui-underline">Action 2</p>
      </PsToast>
      <PsToast v-bind="$props" message="This is a an error alert" layout="error" icon="warning" fill="soft">
        <p class="hover:psui-underline">Action 1</p>
        <p class="hover:psui-underline">Action 2</p>
      </PsToast>
    </div>
  <div>
  `
})

export const Default = Template.bind({})
