import PsClimateZoneBadge from '../components/badges-and-tags/PsClimateZoneBadge.vue'

export default {
  title: 'Components/ClimateZoneBadge',
  component: PsClimateZoneBadge,
}

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { PsClimateZoneBadge },
  template: '<PsClimateZoneBadge v-bind="$props" />',
})

export const ClimateZoneBadge = Template.bind({})
ClimateZoneBadge.args = {
  icon: 'area_chart',
}
