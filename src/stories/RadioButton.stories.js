import PsRadioButton from '../components/controls/PsRadioButton.vue'

export default {
  title: 'Components/Radio Button',
  component: PsRadioButton,
 }

 const DefaultTemplate = (args,{ argTypes}) => ({
   props: Object.keys(argTypes),
   components: { PsRadioButton},
   template: `
    <div style='display: flex; gap: 20px;'>
      <div style='display: flex; flex-direction: column; gap:10px;'>
        <span>Resting</span>
        <div style='display: flex; flex-direction:column; gap:5px;'> 
          <PsRadioButton v-bind='$props' label='Label 1' size='big'/>
          <PsRadioButton v-bind='$props' label='Label 2' size='small'/>
        </div>
      </div>  
      <div style='display: flex; flex-direction: column; gap:10px;'>
        <span>Active</span>
        <div style='display: flex; flex-direction:column; gap:5px;'> 
          <PsRadioButton v-bind='$props' label='Label 3' size='big' checked/>
          <PsRadioButton v-bind='$props' label='Label 4' size='small' checked/>
        </div>
      </div>  
      <div style='display: flex; flex-direction: column; gap:10px;'>
        <span>Disable</span>
        <div style='display: flex; flex-direction:column; gap:5px;'> 
          <PsRadioButton v-bind='$props' label='Label 5' size='big' disabled/>
          <PsRadioButton v-bind='$props' label='Label 6' size='small' disabled/>
        </div>
      </div>  
    </div>  
  `
 })


export const Default = DefaultTemplate.bind({})

