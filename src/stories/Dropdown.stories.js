import PsDropdown from '../components/forms/PsDropdown.vue'
import PsDropdownList from '../components/forms/PsDropdownList.vue'
export default {
  title: 'Components/Dropdown',
  component: { PsDropdown, PsDropdownList },
}

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { PsDropdown, PsDropdownList },
  template: `
  <div style='background: #E6ECF2; width:1200px; height: 700px; padding: 20px;' class='psui-flex psui-gap-64 psui-bg-gray-20 '>
    <PsDropdown>
      <template #dropdownTrigger>
        <i class='psui-icon psui-text-gray-50 psui-cursor-pointer psui-h-2'>more_horiz</i>
      </template>
      <template #items>
        <div class='psui-pb-5'>
        <p class='psui-uppercase psui-text-gray-40 psui-text-accentSmall' style='padding: 24px 20px 8px 20px;'>divider label</p>

        <ul>
          <li style='padding: 7px 20px; font-size:14px' class='hover:psui-bg-blue-10 hover:psui-text-blue-60 psui-text-gray-60 psui-font-bold psui-cursor-pointer'>Selectable option 01</li>
          <li style='padding: 7px 20px; font-size:14px' class='hover:psui-bg-blue-10 hover:psui-text-blue-60 psui-text-gray-60 psui-font-bold psui-cursor-pointer'>Selectable option 02</li>
          <li style='padding: 7px 20px; font-size:14px' class='hover:psui-bg-blue-10 hover:psui-text-blue-60 psui-text-gray-60 psui-font-bold psui-cursor-pointer'>Selectable option 03</li>

          <p class='psui-uppercase psui-text-gray-40 psui-text-accentSmall' style='padding: 24px 24px 8px 24px;'>divider label</p>

          <li style='padding: 7px 20px; font-size:14px' class='hover:psui-bg-blue-10 hover:psui-text-blue-60 psui-text-gray-60 psui-font-bold psui-cursor-pointer'>Selectable option 04</li>
          <li style='padding: 7px 20px; font-size:14px' class='hover:psui-bg-blue-10 hover:psui-text-blue-60 psui-text-gray-60 psui-font-bold psui-cursor-pointer'>Selectable option 05</li>
          <li style='padding: 7px 20px; font-size:14px' class='hover:psui-bg-blue-10 hover:psui-text-blue-60 psui-text-gray-60 psui-font-bold psui-cursor-pointer'>Selectable option 06</li>
        </ul>
        </div>
      </template>
    </PsDropdown>

    <PsDropdown>
    <template #dropdownTrigger>
      <i class='psui-icon psui-text-gray-50 psui-cursor-pointer psui-h-2'>more_horiz</i>
    </template>
    <template #items>
      <div class='psui-pb-5'>
      <p class='psui-uppercase psui-text-gray-40 psui-text-accentSmall' style='padding: 24px 24px 8px 24px;'>divider label</p>

      <ul>
        <li style='padding: 5px 20px; font-size:14px' class='hover:psui-bg-blue-10 hover:psui-text-blue-60 psui-text-gray-60 psui-font-bold psui-cursor-pointer psui-flex psui-items-center psui-gap-3'><i style='font-size: 22px;' class='psui-icon'>public</i><span>Selectable option 01</span></li>
        <li style='padding: 5px 20px; font-size:14px' class='hover:psui-bg-blue-10 hover:psui-text-blue-60 psui-text-gray-60 psui-font-bold psui-cursor-pointer psui-flex psui-items-center psui-gap-3'><i style='font-size: 22px;' class='psui-icon'>share</i><span>Selectable option 02</span></li>
        <li style='padding: 5px 20px; font-size:14px' class='hover:psui-bg-blue-10 hover:psui-text-blue-60 psui-text-gray-60 psui-font-bold psui-cursor-pointer psui-flex psui-items-center psui-gap-3'><i style='font-size: 22px;' class='psui-icon'>person</i><span>Selectable option 03</span></li>

        <p class='psui-uppercase psui-text-gray-40 psui-text-accentSmall' style='padding: 24px 24px 8px 24px;'>divider label</p>

        <li style='padding: 5px 20px; font-size:14px' class='hover:psui-bg-blue-10 hover:psui-text-blue-60 psui-text-gray-60 psui-font-bold psui-cursor-pointer psui-flex psui-items-center psui-gap-3'><i style='font-size: 22px;' class='psui-icon'>settings</i><span>Selectable option 04</span></li>
        <li style='padding: 5px 20px; font-size:14px' class='hover:psui-bg-blue-10 hover:psui-text-blue-60 psui-text-gray-60 psui-font-bold psui-cursor-pointer psui-flex psui-items-center psui-gap-3'><i style='font-size: 22px;' class='psui-icon'>save</i><span>Selectable option 05</span></li>
        <li style='padding: 5px 20px; font-size:14px' class='hover:psui-bg-blue-10 hover:psui-text-blue-60 psui-text-gray-60 psui-font-bold psui-cursor-pointer psui-flex psui-items-center psui-gap-3'><i style='font-size: 22px;' class='psui-icon'>equalizer</i><span>Selectable option 06</span></li>
      </ul>
      </div>
    </template>
  </PsDropdown>

    <PsDropdown>
      <template #dropdownTrigger>
        <i class='psui-icon psui-text-gray-50 psui-cursor-pointer psui-h-auto'>more_horiz</i>
      </template>
      <template #items>
        <PsDropdownList v-bind='$props'></PsDropdownList>
      </template>
    </PsDropdown>

    <PsDropdown>
    <template #dropdownTrigger>
      <i class='psui-icon psui-text-gray-50 psui-cursor-pointer psui-h-auto'>more_horiz</i>
    </template>
    <template #items>
    <div class='psui-my-3'>
    <ul>
    <li style='padding: 5px 20px; font-size:14px' class='hover:psui-bg-blue-10 hover:psui-text-blue-60 psui-text-gray-60 psui-font-bold psui-cursor-pointer psui-flex psui-items-center psui-gap-3'><i style='font-size: 22px;' class='psui-icon'>public</i><span>Selectable option 01</span></li>
    <li style='padding: 5px 20px; font-size:14px' class='hover:psui-bg-blue-10 hover:psui-text-blue-60 psui-text-gray-60 psui-font-bold psui-cursor-pointer psui-flex psui-items-center psui-gap-3'><i style='font-size: 22px;' class='psui-icon'>share</i><span>Selectable option 02</span></li>
    <li style='padding: 5px 20px; font-size:14px' class='hover:psui-bg-blue-10 hover:psui-text-blue-60 psui-text-gray-60 psui-font-bold psui-cursor-pointer psui-flex psui-items-center psui-gap-3'><i style='font-size: 22px;' class='psui-icon'>person</i><span>Selectable option 03</span></li>
    <li style='padding: 5px 20px; font-size:14px' class='hover:psui-bg-blue-10 hover:psui-text-blue-60 psui-text-gray-60 psui-font-bold psui-cursor-pointer psui-flex psui-items-center psui-gap-3'><i style='font-size: 22px;' class='psui-icon'>settings</i><span>Selectable option 04</span></li>
    <li style='padding: 5px 20px; font-size:14px' class='hover:psui-bg-blue-10 hover:psui-text-blue-60 psui-text-gray-60 psui-font-bold psui-cursor-pointer psui-flex psui-items-center psui-gap-3'><i style='font-size: 22px;' class='psui-icon'>save</i><span>Selectable option 05</span></li>
    <li style='padding: 5px 20px; font-size:14px' class='hover:psui-bg-blue-10 hover:psui-text-blue-60 psui-text-gray-60 psui-font-bold psui-cursor-pointer psui-flex psui-items-center psui-gap-3'><i style='font-size: 22px;' class='psui-icon'>equalizer</i><span>Selectable option 06</span></li>
  </ul>
  </div>
    </template>
  </PsDropdown>
  </div>
  `,
})

export const DropdownMenu = Template.bind({})
DropdownMenu.args = {
  items: [
    'Selectable option 01',
    'Selectable option 02',
    'Selectable option 03',
    'Selectable option 04',
    'Selectable option 05',
    'Selectable option 06',
  ],
}
