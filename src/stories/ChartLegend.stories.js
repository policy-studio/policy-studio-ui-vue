import PsChartLegend from '../components/badges-and-tags/PsChartLegend.vue'

export default {
  title: 'Components/ChartLegend',
  component: PsChartLegend,
  argTypes: {},
}

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { PsChartLegend },
  template: '<PsChartLegend v-bind="$props" />',
})

export const ChartLegend = Template.bind({})
ChartLegend.args = {}
