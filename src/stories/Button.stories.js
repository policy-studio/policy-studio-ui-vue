import PsButton, { sizes } from '../components/buttons/PsButton.vue'
const icons = ['add_circle', 'delete', 'done', 'info', 'send']
export default {
  title: 'Components/Button',
  component: PsButton,
  argTypes: {
    size: { control: { type: 'select', options: sizes } },
    disabled: { control: 'boolean' },
    icon: { control: { type: 'select', options: icons } },
    iconRight: { control: { type: 'select', options: icons } },
  },
}

const TemplateDefault = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { PsButton },
  template: `
  <div style="display:flex; flex-direction: column;">
    <p>Size: Big</p> 
    <div style="display: flex; flex-direction: column; gap: 10px;">
      <div style="display: grid; grid-template-columns: repeat(5, minmax(0, 1fr)); gap: 0.5rem; justify-items: center; align-items: center;">
          <span style="text-align: center; color: black;">Solid Style</span>
          <PsButton label='Left Icon'  layout='solid' icon='verified' iconPosition='left'/>
          <PsButton label='Right Icon' layout='solid' icon='verified' iconPosition='right'/>
          <PsButton label='Text Only'  layout='solid' />
          <PsButton label='Disabled'   layout='solid' icon='verified' iconPosition='left' disabled/>
      </div>
      <div style="display: grid; grid-template-columns: repeat(5, minmax(0, 1fr)); gap: 0.5rem; justify-items: center; align-items: center;" >
          <span style="text-align: center; color: blue;">Outline Style</span>
          <PsButton label='Left Icon'  layout='outline' icon='verified' iconPosition='left'/>
          <PsButton label='Right Icon'    layout='outline' icon='verified' iconPosition='right'/>
          <PsButton label='Text Only'  layout='outline'/>
          <PsButton label='Disabled' layout='outline' icon='verified' iconPosition='left' disabled/>
      </div>
      <div style="display: grid; grid-template-columns: repeat(5, minmax(0, 1fr)); gap: 0.5rem; justify-items: center; align-items: center;" >
          <span style="text-align: center; color: brown;">Ghost Style</span>
          <PsButton label='Left Icon' layout='ghost' icon='verified' iconPosition='left'/>
          <PsButton label='Right Icon' layout='ghost' icon='verified' iconPosition='right'/>
          <PsButton label='Text Only' layout='ghost' />
          <PsButton label='Disabled' layout='ghost' icon='verified' iconPosition='left' disabled/>
      </div>
      <div style="display: grid; grid-template-columns: repeat(5, minmax(0, 1fr)); gap: 0.5rem; justify-items: center; align-items: center;" >
          <span style="text-align: center; color: green;">Only-Text Style</span>
          <PsButton label='Left Icon' layout='onlytext' icon='verified' iconPosition='left'/>
          <PsButton label='Right Icon' layout='onlytext' icon='verified' iconPosition='right'/>
          <PsButton label='Text Only' layout='onlytext'/>
          <PsButton label='Disabled' layout='onlytext' icon='verified' iconPosition='left' disabled/>
      </div>
      <div style="display: grid; grid-template-columns: repeat(5, minmax(0, 1fr)); gap: 0.5rem; justify-items: center; align-items: center;" >
          <span style="text-align: center; color: red;">Caution Button</span>
          <PsButton label='Left Icon' layout='caution' icon='verified' iconPosition='left'/>
          <PsButton label='Right Icon' layout='caution' icon='verified' iconPosition='right'/>
          <PsButton label='Text Only' layout='caution' />
          <PsButton label='Disabled' layout='caution' icon='verified' iconPosition='left' disabled/>
    </div>


    </div>
    <p>Size: Medium</p>
    <div style="display: flex; flex-direction: column; gap: 10px;">
      <div style="display: grid; grid-template-columns: repeat(5, minmax(0, 1fr)); gap: 0.5rem; justify-items: center; align-items: center;" >
          <span style="text-align: center; color: black;">Solid Style</span>
          <PsButton label='Left Icon' layout='solid' icon='verified' iconPosition='left' size='medium'/>
          <PsButton label='Right Icon' layout='solid' icon='verified' iconPosition='right' size='medium'/>
          <PsButton label='Text Only' layout='solid'  size='medium'/>
          <PsButton label='Disabled' layout='solid' icon='verified' iconPosition='left' disabled size='medium'/>
      </div>
      <div style="display: grid; grid-template-columns: repeat(5, minmax(0, 1fr)); gap: 0.5rem; justify-items: center; align-items: center;" >
          <span style="text-align: center; color: blue;">Outline Style</span>
          <PsButton label='Left Icon' layout='outline' icon='verified' iconPosition='left' size='medium'/>
          <PsButton label='Right Icon' layout='outline' icon='verified' iconPosition='right' size='medium'/>
          <PsButton label='Text Only' layout='outline' size='medium'/>
          <PsButton label='Disabled' layout='outline' icon='verified' iconPosition='left' disabled size='medium'/>
      </div>
      <div style="display: grid; grid-template-columns: repeat(5, minmax(0, 1fr)); gap: 0.5rem; justify-items: center; align-items: center;" >
          <span style="text-align: center; color: brown;">Ghost Style</span>
          <PsButton label='Left Icon' layout='ghost' icon='verified' iconPosition='left' size='medium'/>
          <PsButton label='Right Icon' layout='ghost' icon='verified' iconPosition='right' size='medium'/>
          <PsButton label='Text Only' layout='ghost' size='medium'/>
          <PsButton label='Disabled' layout='ghost' icon='verified' iconPosition='left' disabled size='medium'/>
      </div>
      <div style="display: grid; grid-template-columns: repeat(5, minmax(0, 1fr)); gap: 0.5rem; justify-items: center; align-items: center;" >
          <span style="text-align: center; color: green;">Only-Text Style</span>
          <PsButton label='Left Icon' layout='onlytext' icon='verified' iconPosition='left' size='medium'/>
          <PsButton label='Right Icon' layout='onlytext' icon='verified' iconPosition='right' size='medium'/>
          <PsButton label='Text Only' layout='onlytext' size='medium'/>
          <PsButton label='Disabled' layout='onlytext' disabled icon='verified' iconPosition='left' size='medium'/>
      </div>
      <div style="display: grid; grid-template-columns: repeat(5, minmax(0, 1fr)); gap: 0.5rem; justify-items: center; align-items: center;" >
          <span style="text-align: center; color: red;">Caution Button</span>
          <PsButton label='Left Icon' layout='caution' icon='verified' iconPosition='left' size='medium'/>
          <PsButton label='Right Icon' layout='caution' icon='verified' iconPosition='right' size='medium'/>
          <PsButton label='Text Only' layout='caution' size='medium'/>
          <PsButton label='Disabled' layout='caution' icon='verified' iconPosition='left' disabled size='medium'/>
      </div>      
    </div>
    <p>Size: Small</p>
    <div style="display: flex; flex-direction: column; gap: 10px;">
      <div style="display: grid; grid-template-columns: repeat(5, minmax(0, 1fr)); gap: 0.5rem; justify-items: center; align-items: center;" >
          <span style="text-align: center; color: black;">Solid Style</span>
          <PsButton label='Left Icon' layout='solid' icon='verified' iconPosition='left' size='small'/>
          <PsButton label='Right Icon' layout='solid' icon='verified' iconPosition='right' size='small'/>
          <PsButton label='Text Only' layout='solid' size='small'/>
          <PsButton label='Disabled' layout='solid' icon='verified' iconPosition='left' disabled size='small'/>
      </div>
      <div style="display: grid; grid-template-columns: repeat(5, minmax(0, 1fr)); gap: 0.5rem; justify-items: center; align-items: center;" >
          <span style="text-align: center; color: green;">Only-Text Style</span>
          <PsButton label='Left Icon' layout='onlytext' icon='verified' iconPosition='left' size='small'/>
          <PsButton label='Right Icon' layout='onlytext' icon='verified' iconPosition='right' size='small'/>
          <PsButton label='Text Only' layout='onlytext' size='small'/>
          <PsButton label='Disabled' layout='onlytext' disabled icon='verified' iconPosition='left' size='small'/>
      </div>
      <div style="display: grid; grid-template-columns: repeat(5, minmax(0, 1fr)); gap: 0.5rem; justify-items: center; align-items: center;" >
          <span style="text-align: center; color: red;">Caution Button</span>
          <PsButton label='Left Icon' layout='caution' icon='verified' iconPosition='left' size='small'/>
          <PsButton label='Right Icon' layout='caution' icon='verified' iconPosition='right' size='small'/>
          <PsButton label='Text Only' layout='caution' size='small'/>
          <PsButton label='Disabled' layout='caution' disabled icon='verified' iconPosition='left' size='small'/>
      </div>
    </div>
    </div>
  `,
})

export const Default = TemplateDefault.bind({})
