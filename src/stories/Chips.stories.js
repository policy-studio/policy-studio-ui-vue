import PsChips from '../components/chips/PsChips.vue'

export default {
  title: 'Components/Chips',
  component: PsChips,
}

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { PsChips },
  data: ()=>{
    return{
      isChecked: null,
      isCheckedTwo: null,
    }
  },
  template: `<div style="display:flex; gap: 10px;">
                <PsChips v-bind='$props' type="button" @update:checked='isChecked = $event' :checked='isChecked'/>
                <PsChips v-bind='$props' label="Simple chips with icons" type="button" layout="with-icon" icon="home" @update:checked='isChecked = $event' :checked='isChecked'/>
                <PsChips v-bind='$props' label="Radio chips" type="radio" @update:checked='isChecked = $event' :checked='isChecked'/>
                <PsChips v-bind='$props' label="Checkbox chips" type="checkbox" @update:checked='isCheckedTwo = $event' :checked='isCheckedTwo'/>
                <PsChips v-bind='$props' label="Rich chips" type="button" layout="rich" icon="text_snippet" @update:checked='isChecked = $event' :checked='isChecked'/>
              </div>
              `
})

export const Default = Template.bind({})
Default.args = {
  label: 'Simple Chip',
  icon:'',
}