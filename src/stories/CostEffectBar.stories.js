import PsCostEffectBar from '../components/badges-and-tags/PsCostEffectBar.vue'

export default {
  title: 'Components/CostEffectBar',
  component: PsCostEffectBar,
  argTypes: {
    value: {
      control: { type: 'number', min: 0, max: 100 },
    },
    breakEven: {
      control: { type: 'number', min: 0, max: 100 },
    },
  },
}

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { PsCostEffectBar },
  template: '<PsCostEffectBar v-bind="$props" />',
})

export const SimpleProgressBar = Template.bind({})
SimpleProgressBar.args = {}
