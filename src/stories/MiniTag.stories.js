import PsMiniTag, {
  typeOptions,
} from '../components/badges-and-tags/PsMiniTag.vue'

export default {
  title: 'Components/MiniTag',
  component: PsMiniTag,
  argTypes: {
    type: {
      control: {
        type: 'select',
        options: typeOptions,
      },
    },
  },
}

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { PsMiniTag },
  template: '<PsMiniTag v-bind="$props" />',
})

export const Info = Template.bind({})
Info.args = {
  layout: 'info',
  message: 'This is an info alert',
}

export const Success = Template.bind({})
Success.args = {
  layout: 'success',
  message: 'This is a success alert',
}

export const Warning = Template.bind({})
Warning.args = {
  layout: 'warning',
  message: 'This is a warning alert',
}

export const Error = Template.bind({})
Error.args = {
  layout: 'error',
  message: 'This is an error alert',
}

export const Default = Template.bind({})
Default.args = {
  layout: 'default',
  message: 'This is a default color',
}
