import PsCheckbox from '../components/controls/PsCheckbox.vue'
// const icons = ['add_circle', 'delete', 'done', 'info', 'send']
export default {
  title: 'Components/Checkbox',
  component: PsCheckbox,
}

const defaultTemplate = (args, {argTypes}) => ({
  props: Object.keys(argTypes),
  components: { PsCheckbox},
  template: `
    <div style='display:flex; gap: 20px;'>
      <div style='display:flex; flex-direction:column; gap:5px;'>
        <p>Resting</p>
        <div style='display: flex; flex-direction:column; gap: 10px;'>
          <PsCheckbox v-bind="$props" label='Label 1' />
          <PsCheckbox v-bind="$props" label='Label 2' size='small' />
        </div>
      </div>
      <div style='display:flex; flex-direction:column; gap:5px;'>
        <p>Active</p>
        <div style='display: flex; flex-direction:column; gap: 10px;'>
          <PsCheckbox v-bind="$props" label='Label 3'  checked />
          <PsCheckbox v-bind="$props" label='Label 4'  checked size='small'/>
        </div>
      </div>
      <div style='display:flex; flex-direction:column; gap:5px;'>
        <p>Mixed</p>
        <div style='display: flex; flex-direction:column; gap: 10px;'>
          <PsCheckbox v-bind="$props" label='Label 5' layout='mixed' checked />
          <PsCheckbox v-bind="$props" label='Label 6' layout='mixed' size='small' checked />
        </div>
      </div>
      <div style='display:flex; flex-direction:column; gap:5px;'>
        <p>Disabled</p>
        <div style='display: flex; flex-direction:column; gap: 10px;'>
          <PsCheckbox v-bind="$props" label='Label 7' disabled/>
          <PsCheckbox v-bind="$props" label='Label 8' size='small' disabled/>
        </div>
      </div>
    </div>
    `
})

export const Default = defaultTemplate.bind({}) 