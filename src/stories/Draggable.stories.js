import PsDraggable from '../components/controls/PsDraggable.vue'

export default {
    title: 'Components/Draggable',
    component: PsDraggable 
}

const Template = (args, {argTypes}) => ({
    props: Object.keys(argTypes),
    components: { PsDraggable },
    template: `
        <div style='width: 300px; font-family: "Lato", sans-serif; font-size: 12px;'>
            <PsDraggable v-bind='$props'/>
        </div>
            `
})

export const Component = Template.bind({})
Component.args = {
    getColumns: {columnGroups:[{title: 'COST EFFECTIVENESS', columns: [ 'Teste1' , 'Teste2' , 'Teste3', 'Teste4']}, {title: 'PER HOME RESULTS', columns: [ 'Teste5' , 'Teste6' , 'Teste7', 'Teste8']}]},
    module: 'comparison'
}