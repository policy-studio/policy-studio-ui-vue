import PsIcon from '../components/ui/PsIcon.vue'

export default {
    title: 'Components/Icon',
    component: PsIcon,
    argTypes: {},
  }
  
  const Template = (args, { argTypes }) => ({
    props: Object.keys(argTypes),
    components: { PsIcon },
    template: `
    <div class='psui-flex'>  
      <PsIcon v-bind="$props" type='svg' icon="/images/multifamily-units.svg" />
      <PsIcon v-bind="$props" type='svg' icon="/images/multifamily-units.svg"/>
    </div>
    `,
  })
  
  export const IconSimple = Template.bind({})
  IconSimple.args = {}