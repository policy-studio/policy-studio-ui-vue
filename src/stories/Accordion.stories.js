import PsAccordionItem  from '../components/accordion/PsAccordionItem.vue'
import PsAccordion from '../components/accordion/PsAccordion.vue'

export default {
  title: 'Components/Accordion',
  component: PsAccordion,
  subcomponents: { PsAccordionItem },
}


export const Index = (args, { argTypes }) => ({
  props: Object.keys(argTypes, args),
  components: { PsAccordion, PsAccordionItem },
  template:
    `
    <div class="psui-p-8 psui-bg-gray-10 psui-grid psui-grid-cols-2 psui-gap-6">                    
      <div>
        <div class="psui-my-4">
          <h1 class="psui-font-bold psui-border-b psui-border-gray-30">Accordion Big</h1>
        </div>
        <PsAccordion layout="big" class="psui-bg-white">
          <PsAccordionItem title="Header 01">
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates, illum.</p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates, illum.</p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates, illum.</p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates, illum.</p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates, illum.</p>
          </PsAccordionItem>      
          <PsAccordionItem title="Header 02">
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates, illum.</p>
          </PsAccordionItem>      
          <PsAccordionItem title="Header 03">
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates, illum.</p>
          </PsAccordionItem>      
        </PsAccordion>
      </div>

      <div>
        <div class="psui-my-4">
          <h1 class="psui-font-bold psui-border-b psui-border-gray-30">Accordion Medium</h1>
        </div>
        <PsAccordion layout="medium" class="psui-bg-white">
          <PsAccordionItem title="Header 01">
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates, illum.</p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates, illum.</p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates, illum.</p>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates, illum.</p>
          </PsAccordionItem>      
          <PsAccordionItem title="Header 02" :opened="false">
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates, illum.</p>
          </PsAccordionItem>      
          <PsAccordionItem title="Header 03">
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates, illum.</p>
          </PsAccordionItem>      
        </PsAccordion>
      </div>
    </div>  
    `,
})
