import PsTabHeader from '../components/tabs/PsTabHeader.vue'
const items = ['Existing Buildings', 'New Constructions', 'Other tab']
const item = items[0]

export default {
  title: 'Components/Tab Header',
  component: PsTabHeader,
  argTypes: {}
}
const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { PsTabHeader },
  data: () => {
    return {
    }
  },
  template: `
  <div class="psui-bg-gray-20 psui-p-8">
    <PsTabHeader :selected.sync=selected v-bind="$props"/>
    <div v-if="$props['selected'] === 'Existing Buildings'" class="psui-bg-white psui-p-4" :class="{ 'psui-mt-4' : $props['layout'] === 'standard'}">
      <p v-for="i of 4">Tab Existing Buildings Selected</p>
    </div>
    <div v-if="$props['selected'] === 'New Constructions'" class="psui-bg-white psui-p-4" :class="{ 'psui-mt-4' : $props['layout'] === 'standard'}">
      <p v-for="i of 4">Tab New Constructions Selected</p>
    </div>
    <div v-if="$props['selected'] === 'Other tab'" class="psui-bg-white psui-p-4" :class="{ 'psui-mt-4' : $props['layout'] === 'standard'}">
      <p v-for="i of 4">Other tab Selected</p>
    </div>
  </div>
  `
})

export const Standard = Template.bind({})
Standard.args = {
  layout: 'standard',
  items: items,
  selected: item
}

export const Underline = Template.bind({})
Underline.args = {
  layout: 'underline',
  items: items,
  selected: item
}

export const Folder = Template.bind({})
Folder.args = {
  layout: 'folder',
  items: items,
  selected: item
}
