import PsToggle from '../components/controls/PsToggle.vue'
export default {
  title: 'Components/Toggle',
  component: PsToggle,
  argTypes: {},
}
const items = ['Option 1', 'Option 2', 'Option 3']
const selected = 'Option 2'

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { PsToggle },
  template: `
  <PsToggle v-bind="$props" :selected.sync="selected" />
  `,
})

export const Default = Template.bind({})
Default.args = {
  items: items,
  selected: selected,
}

/**
 * Hey Luciano!
 * Adicionei este script aqui pois não entendi como criar uma story com script em MDX ou algo que rode geral, saca?
 * Mas é uma função simples para copiar o HEX das cores...
 * Se quiser dar uma melhorada nessa parte, fique a vontade meu chegado! TMJ
 */
window.addEventListener('click', (ev) => {
  if (ev.target.classList.contains('click-to-copy')) {
    copyText(ev.target.dataset.toCopy)
  }
})

function copyText(textToCopy) {
  var myTemporaryInputElement = document.createElement('input')
  myTemporaryInputElement.type = 'text'
  myTemporaryInputElement.value = textToCopy
  document.body.appendChild(myTemporaryInputElement)
  myTemporaryInputElement.select()
  document.execCommand('Copy')
  document.body.removeChild(myTemporaryInputElement)
  alert('Item copied to clipboard!')
}
