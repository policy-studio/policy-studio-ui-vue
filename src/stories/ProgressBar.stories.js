import PsProgressBar from '../components/badges-and-tags/PsProgressBar.vue'

export default {
  title: 'Components/ProgressBar',
  component: PsProgressBar,
  argTypes: {
    value: {
      control: { type: 'number', min: 0, max: 100 },
    },
    breakEven: {
      control: { type: 'number', min: 0, max: 100 },
    },
  },
}

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { PsProgressBar },
  template: '<PsProgressBar v-bind="$props" />',
})

export const ProgressBar = Template.bind({})
ProgressBar.args = {}
