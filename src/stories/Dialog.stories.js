import PsDialog from '../components/notifications/PsDialog.vue'

export default {
  title: 'Components/Dialog',
  component: PsDialog,
}

const TemplateExamples = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { PsDialog },
  template: `
          <div class="psui-grid psui-grid-cols-3 psui-gap-4">
            
            <div class="psui-col-span-3 psui-mt-8">
              <h1 class="psui-font-bold psui-border-b psui-border-gray-30">PSDialog Horizontal</h1>
            </div>

            <div>
              <h2>Informative</h2>
              <PsDialog theme="informative" layout="horizontal" message="teste">                
                <template v-slot:action>
                  <p style='font-weight: 700;'>Action</p>
                </template>
              </PsDialog>
            </div>
            
            <div>
              <h2>Success</h2>
              <PsDialog theme="success" layout="horizontal" :hasClose="false">
                <template>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </template>
                <template v-slot:action>
                  <p style='font-weight: 700;'>Action</p>
                </template>
              </PsDialog>
            </div>
            
            <div>
              <h2>Alert</h2>
              <PsDialog theme="alert" layout="horizontal">
                <template>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </template>
                <template v-slot:action>
                  <p style='font-weight: 700;'>Action</p>
                </template>
              </PsDialog>
            </div>

            <div class="psui-col-span-3 psui-mt-8">
              <h1 class="psui-font-bold psui-border-b psui-border-gray-30">PSDialog Vertical</h1>
            </div>

            <div>
              <h2>Informative</h2>
              <PsDialog theme="informative" layout="vertical">
                <template>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </template>
                <template v-slot:action>
                  <p style='font-weight: 700;'>Action</p>
                </template>
              </PsDialog>
            </div>
            
            <div class="psui-text-red">
              <h2>Success</h2>
              <PsDialog theme="success" layout="vertical" :hasClose="false">
                <template>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </template>
                <template v-slot:action>
                  <p style='font-weight: 700;'>Action</p>
                </template>
              </PsDialog>
            </div>
            
            <div>
              <h2>Alert</h2>
              <PsDialog theme="alert" layout="vertical">
                <template>
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </template>
                <template v-slot:action>
                  <p style='font-weight: 700;'>Action</p>
                </template>
              </PsDialog>
            </div>

            
            
            <!-- PSDialog Vertical -->
            <div class="psui-col-span-3 psui-mt-8">
              <h1 class="psui-font-bold psui-border-b psui-border-gray-30">PSDialog Vertical Props Message</h1>
            </div>

            <div>
              <h2>Informative</h2>
              <PsDialog theme="informative" layout="vertical" message="This component uses only the props message!">
                <template v-slot:action>
                  <p style='font-weight: 700;'>Action</p>
                </template>
              </PsDialog>
            </div>
            
            <div>
              <h2>Success</h2>
              <PsDialog theme="success" layout="vertical" message="This component uses only the props message!" :hasClose="false">
                <template v-slot:action>
                  <p style='font-weight: 700;'>Action</p>
                </template>
              </PsDialog>
            </div>
            
            <div>
              <h2>Alert</h2>
              <PsDialog theme="alert" layout="vertical" message="This component uses only the props message!">
                <template v-slot:action>
                  <p style='font-weight: 700;'>Action</p>
                </template>
              </PsDialog>
            </div>

          </div>
          `,
})

export const Examples = TemplateExamples.bind({})


