import PsHighlightRippleDot from '../components/badges-and-tags/PsHighlightRippleDot.vue'

export default {
  title: 'Components/HighlightRippleDot',
  component: PsHighlightRippleDot,
  argTypes: {},
}

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { PsHighlightRippleDot },
  template: '<PsHighlightRippleDot v-bind="$props" />',
})

export const HighlightRippleDot = Template.bind({})
HighlightRippleDot.args = {}
