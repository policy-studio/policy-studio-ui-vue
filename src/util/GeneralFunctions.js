export const randomString = (length) => {
  var result = ''
  var characters =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
  var charactersLength = characters.length
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength))
  }
  return result
}

export const getParentScrollableEl = (node) => {
  if (node == null) {
    return null
  }

  if (node.scrollHeight > node.clientHeight) {
    return node
  } else {
    return getParentScrollableEl(node.parentNode)
  }
}

export const getParentVueComponentByName = (comp, name) => {
  if (comp.$options.name === name) {
    return comp
  } else {
    return getParentVueComponentByName(comp.$parent, name)
  }
}
