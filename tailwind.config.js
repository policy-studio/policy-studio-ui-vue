module.exports = {
  prefix: 'psui-',
  purge: ['./index.html', './src/**/*.vue,js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      'white': '#ffffff !important',
      'gray-10': '#F3F6F9',
      'gray-20': '#E6ECF2',
      'gray-30': '#D6DDE5',
      'gray-40': '#A2ACB7',
      'gray-50': '#798490',
      'gray-60': '#515E6A',
      'gray-70': '#34404A',
      'gray-80': '#28323B',

      'blue': '#5094D3',
      'blue-10': '#ECF7FB',
      'blue-20': '#E0EFF6',
      'blue-50': '#64B5CE',
      'blue-60': '#318FAC',
      'blue-70': '#00465F',
      'blue-80': '#002A3A',

      'green-10': '#DEF8E8',
      'green-20': '#5DB883',
      'green-70': '#44A06A',
      'green-80': '#286943',

      'yellow-10': '#FDF3E3',
      'yellow-20': '#EDAB3E',
      'yellow-70': '#B87305',
      'yellow-80': '#834C0D',

      'red-10': '#FCEBEB',
      'red-15': '#EAC5C5',
      'red-20': '#D65C5A',
      'red-70': '#AA3937',
      'red-80': '#832F2E',

      sky: '#518BE2',
      teal: '#57C0BA',
      emerald: '#8CCA82',
      mustard: '#E9CF74',
      orange: '#FF906D',
      pink: '#FF77B8',
      purple: '#9278C9',
      transparent: 'transparent',
    },
    fontFamily: {
      sans: ['Lato'],
    },
    fontSize: {
      big: ['16px', '130%'],
      small: ['14px', '130%'],
      xsmall: ['12px', '130%'],
      accent: ['14px', '130%', { letterSpacing: '0.6px' }],
      accentSmall: ['12px', '130%', { letterSpacing: '0.6px' }],
      p: ['16px', '130%'],
      h1: ['28px', '130%'],
      h2: ['24px', '130%'],
      h3: ['22px', '120%'],
      h4: ['20px', '120%'],
      h5: ['18px', '120%'],
      h6: ['17px', '120%'],
    },
    boxShadow: {
      'elevation--5':
        '0px 1px 0px #FFFFFF, inset 0px 1px 2px rgba(0, 0, 0, 0.1)',
      'elevation-5':
        '0px 0px 4px rgba(0, 0, 0, 0.03), 0px 1px 2px rgba(0, 0, 0, 0.1)',
      'elevation-10':
        '0px 0px 8px rgba(0, 0, 0, 0.04), 0px 2px 5px rgba(0, 0, 0, 0.08)',
      'elevation-20':
        '0px 0px 8px rgba(0, 0, 0, 0.04), 0px 5px 6px rgba(0, 0, 0, 0.1)',
      'elevation-30':
        '0px 0px 8px rgba(0, 0, 0, 0.05), 0px 10px 15px rgba(0, 0, 0, 0.15)',
      'elevation-40':
        '0px 0px 20px rgba(0, 0, 0, 0.05), 0px 30px 60px rgba(0, 0, 0, 0.2)',
      sm: '0px 0px 4px rgba(0, 0, 0, 0.03), 0px 1px 2px rgba(0, 0, 0, 0.1)',
      default:
        '0px 0px 8px rgba(0, 0, 0, 0.04), 0px 2px 5px rgba(0, 0, 0, 0.08)',
      md: '0px 0px 8px rgba(0, 0, 0, 0.04), 0px 5px 6px rgba(0, 0, 0, 0.1)',
      lg: '0px 0px 8px rgba(0, 0, 0, 0.05), 0px 10px 15px rgba(0, 0, 0, 0.15)',
      xl: '0px 0px 20px rgba(0, 0, 0, 0.05), 0px 30px 60px rgba(0, 0, 0, 0.2)',
      inner: '0px 1px 0px #FFFFFF, inset 0px 1px 2px rgba(0, 0, 0, 0.1)',
      none: 'none',
    },
    borderColor: (theme) => ({
      ...theme('colors'),
      DEFAULT: theme('colors.blue-70', 'currentColor'),
    }),
    divideColor: (theme) => ({
      ...theme('colors'),
    }),
    extend: {
      divideOpacity: {
        10: '0.1',
        20: '0.2',
        95: '0.95',
      },
      cursor: {
        'grab': 'grab',
        'grabbing': 'grabbing'
      }
    },
  },
  variants: {
    boxShadow: ['active'],
    borderRadius: ['last', 'first'],
    backgroundColor: [
      'responsive',
      'hover',
      'active',
      'focus',
      'group-hover',
      'even',
      'first',
      'disabled',
      'checked',
    ],
    textColor: [
      'responsive',
      'hover',
      'active',
      'focus',
      'group-hover',
      'first',
      'disabled',
      'checked',
    ],
    borderColor: [
      'disabled',
      'hover',
      'checked'
    ],
    extend: {},
  },
  plugins: [],
}
